#include <Arduino.h>
#include <OpenTherm.h>
#include <DHT.h>

#define DHTPIN 13
#define DHTTYPE DHT22

const int inPin = 4; // 12; // GPIO## pin number, grey
const int outPin = 5; // 13; // purple 
OpenTherm ot(inPin, outPin);

DHT dht(DHTPIN, DHTTYPE);

void handleInterrupt() {
	ot.handleInterrupt();
}

void setup()
{
	Serial.begin(115200);
	Serial.println("Start");
	
	ot.begin(handleInterrupt);
	Serial.println("Interrupt handler setup OK.");

	dht.begin();
}

unsigned int data = 0xFFFF;
unsigned int fanLevel = 0x14;
int i=0;

void loop()
{	
	float hum = dht.readHumidity();
	float temp = dht.readTemperature();

	Serial.println("Humidity: " + String(hum) + " Temp: " + String(temp));
	Serial.println("i=" + String(i));

// Increment fan level, once per minute
   if (i++ > 5) {
		unsigned long request4 = ot.buildRequest(
			OpenThermRequestType::WRITE,
			OpenThermMessageID::CONTROL_SETPOINT_VH,
			fanLevel);
		Serial.print("Setting fan level to: " + String(fanLevel) + " : ");
		unsigned long response4 = ot.sendRequest(request4);
		Serial.println("Response: " + String(response4, HEX));
	//	fanLevel += 5;
	//	if (fanLevel > 150) fanLevel = 0;
		i=0;
    
		OpenThermResponseStatus responseStatus4 = ot.getLastResponseStatus();
		if (responseStatus4 == OpenThermResponseStatus::SUCCESS) {		
			Serial.println("Success.");
		}
		if (responseStatus4 == OpenThermResponseStatus::NONE) {
			Serial.println("Error: OpenTherm is not initialized");
		}
		else if (responseStatus4 == OpenThermResponseStatus::INVALID) {
			Serial.println("Error: Invalid response " + String(response4, HEX));
		}
		else if (responseStatus4 == OpenThermResponseStatus::TIMEOUT) {
			Serial.println("Error: Response timeout");
		}
		
		Serial.println("Requesting TSP Parameters:");
		for (int j=0; j<64; j++) {
			data = j * 256;
			unsigned long getTSP = ot.buildRequest(
				OpenThermRequestType::READ,
				OpenThermMessageID::TSP_ENTRY_VH,
				data);
			int resultTSP = ot.sendRequest(getTSP) & 0xFFFF;
			if (ot.getLastResponseStatus() != OpenThermResponseStatus::SUCCESS)
				resultTSP = -1;
			Serial.printf("%04X, ", resultTSP);
			if (j % 8 == 7) Serial.println();
		}

	}

  //Read Relative Ventilation Level:

    unsigned long request = ot.buildRequest(
      OpenThermRequestType::READ,
      OpenThermMessageID::RELATIVE_VENTILATION,
      data);
	Serial.print("Reading fan level: ");
    unsigned long response2 = ot.sendRequest(request);
    Serial.println(String(response2, HEX) + " = " + String(response2 & 0xFF)+"%");
	// 	Serial.print("Getting response: ");
	// OpenThermResponseStatus responseStatus2 = ot.getLastResponseStatus();
	// if (responseStatus2 == OpenThermResponseStatus::SUCCESS) {		
	// 	Serial.println("Success.");
	// }
	// if (responseStatus2 == OpenThermResponseStatus::NONE) {
	// 	Serial.println("Error: OpenTherm is not initialized");
	// }
	// else if (responseStatus2 == OpenThermResponseStatus::INVALID) {
	// 	Serial.println("Error: Invalid response " + String(response2, HEX));
	// }
	// else if (responseStatus2 == OpenThermResponseStatus::TIMEOUT) {
	// 	Serial.println("Error: Response timeout");
	// }

    unsigned long getInletTemp = ot.buildRequest(
      OpenThermRequestType::READ,
      OpenThermMessageID::SUPPLY_INLET_TEMP,
      data);
	unsigned long inletTemp = ot.sendRequest(getInletTemp);
	float inTempConverted = (float)(inletTemp & 0xFFFF) / 256;
	Serial.println("Got Inlet Temp: " + String (inletTemp, HEX) + " = " +
	  String((inTempConverted)));

    unsigned long getOutletTemp = ot.buildRequest(
      OpenThermRequestType::READ,
      OpenThermMessageID::EXHAUST_INLET_TEMP,
      data);
	unsigned long outletTemp = ot.sendRequest(getOutletTemp);
	float outTemp = (float)(outletTemp & 0xFFFF) / 256;
	Serial.println("Got Outlet Temp: " + String (outletTemp, HEX) + " = " +
	  String((outTemp)));

	Serial.println();
	delay(1000);
}